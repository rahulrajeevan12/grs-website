module.exports = function(grunt) {

	var LIVERELOAD_PORT = 35729;
	process.env.devPort = 8999;
	process.env.buildPort = 9001;

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		clean: {
			all: [
				'dist/**'
			],
			temp: [
				'dist/grs.all.js'
			]
		},
		copy: {
			main: {
				files: [
					{
						expand: true,
						cwd: 'src/main',
						src: [
							'php/**',
							'templates/**',
							'assets/img/**',
							'assets/fonts/**',
						],
						dest: 'dist/'
					}
				]
			},
			singles: {
				options: {
					flatten: true
				},
				files: {
					'dist/index.html': 'src/main/index-build.html',
					'dist/assets/css/grs-app.css': 'src/main/assets/css/style.min.css',
                     'dist/assets/css/bootstrap.min.css':'src/main/assets/css/bootstrap.min.css',
                     'dist/assets/css/responsive.min.css':'src/main/assets/css/responsive.min.css',
					'dist/assets/css/pages.css':'src/main/assets/css/pages.css',
					'dist/assets/css/home-pages-customizer.css':'src/main/assets/css/home-pages-customizer.css',
				}
			}
		},
		concat: {
			custom: {
				options: {
					separator: ';'
				},
				dest: 'dist/grs.all.js',
				src: [
					'src/main/app.js',
				]
			},
			vendor: {
				options: {
					separator: ';\n'
				},
				dest: 'dist/vendor.min.js',
				src: [
						'src/main/lib/js/angular.min.js',
						'src/main/lib/js/jquery.min.js',
						'src/main/lib/js/bootstrap.min.js',
						'src/main/lib/js/jquery.carouFredSel-6.2.1-packed.js',
						'src/main/lib/js/jquery.touchSwipe.min.js',
						'src/main/lib/js/jquery.elevateZoom-3.0.8.min.js',
						'src/main/lib/js/jquery.imagesloaded.min.js',
						'src/main/lib/js/jquery.fancybox.pack.js',
						'src/main/lib/js/isotope.pkgd.min.js',
						'src/main/lib/js/jquery.stellar.min.js',       
						'src/main/lib/js/jquery.tubular.1.0.js',
						'src/main/lib/js/SmoothScroll.js',
						'src/main/lib/js/spin.min.js',
						'src/main/lib/js/masonry.pkgd.min.js',
						'src/main/lib/js/morris.min.js',
						'src/main/lib/js/video.js',
						'src/main/lib/js/pixastic.custom.js',
						'src/main/lib/js/jquery.themepunch.revolution.min.js',
						'src/main/lib/js/main.js'
				]
			},
			vendorCSS: {
				options: {
					separator: "\n"
				},
				dest: 'dist/assets/css/vendor.css',
				src: [
					'src/main/assets/css/social-icons-codes.min.css',
					'src/main/assets/css/buttons.min.css',
					'src/main/assets/css/font-awesome.min.css',
					'src/main/assets/css/settings.min.css',
					'src/main/assets/css/jquery.fancybox.min.css',
					'src/main/assets/css/animate.min.css',
					'src/main/assets/css/video-js.min.css',
					'src/main/assets/css/morris.min.css',
					'src/main/assets/css/royalslider.min.css',
					'src/main/assets/css/rs-minimal-white.min.css',
					'src/main/assets/css/ie.css'
				]
			}
		},
		uglify: {
			custom: {
				files: {
					'dist/grs.min.js': ['dist/grs.all.js']
				}
			}
		},
		connect: {
			build: {
				options: {
					port: 9001,
					hostname: '0.0.0.0',
					base: 'dist',
					open: true
				}
			},
			dev: {
				options: {
					port: 8999,
					livereload: LIVERELOAD_PORT,
					hostname: '0.0.0.0',
					base: 'src/main'
				}
			}
		},
		open: {
			dev: {
				path: 'http://localhost:<%= connect.dev.options.port %>'
			},
			build: {
				path: 'http://localhost:<%= connect.build.options.port %>'
			}
		}
		
		
	});

	grunt.loadNpmTasks('grunt-open');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('cleanAll', ['clean:all']);
	grunt.registerTask('build', ['clean:all', 'concat:custom', 'uglify', 'concat:vendor', 'concat:vendorCSS', 'copy', 'clean:temp']);
	
	
	
};