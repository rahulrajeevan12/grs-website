angular.module('grsApp', ['ngRoute', 'grsweb.common','grsweb.modules'])
        .config(['$routeProvider',
            function($routeProvider) {
                $routeProvider
                        .when('/', {templateUrl: 'modules/home/home.html', controller: 'homeCtrl'})
                        .when('/Aboutus', {templateUrl: 'modules/aboutus/aboutus.html', controller: 'aboutusCtrl'})
                        .when('/ContactUs', {templateUrl: 'modules/contactus/contactus.html', controller: 'aboutusCtrl'})
                        .when('/products', {templateUrl: 'modules/products/product-list.html', controller: 'productListCtrl'})
                        .when('/Brands', {templateUrl: 'modules/brands/brands.html', controller: 'brandsCtrl'})
                        .when('/showroom', {templateUrl: 'modules/showroom/showroom.html', controller: 'showroomCtrl'})
                        .when('/products/:productId', {templateUrl: 'modules/products/product-detail.html', controller: 'productDetailCtrl'})
                        .when('/newsandevents', {templateUrl: 'modules/history/history.html',controller: 'historyCtrl'})
                        .otherwise({redirectTo: '/'});
                //$locationProvider.html5Mode(true);
                //$locationProvider.hashPrefix('!');
            }])
        .controller('mainController', function($scope) {
            $scope.seo = {
                pageTitle: '',
                pageDescription: ''
            };
        });
