angular.module('grsweb.common')
        .factory('Product', ['$resource',
            function($resource) {
                return $resource('common/data/products/:productId.json', {}, {
                    query: {method: 'GET', params: {productId: 'products'}, isArray: true}
                });
            }]);

