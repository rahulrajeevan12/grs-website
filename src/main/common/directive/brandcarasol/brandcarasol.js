angular.module('grsweb.common')
        .directive('brandCarasol', ['$rootScope', function($rootScope) {
                return {
                    restrict: 'AE',
                    replace: true,
                    templateUrl: 'common/directive/brandcarasol/brandCarasol.html',
                    link: function($scope) {
                        var $ = jQuery;
                        if ($('.carousel-box .carousel').length) {
                            var carouselBox = $('.carousel-box .carousel');

                            carouselBox.each(function() {
                                var carousel = $(this).closest('.carousel-box'),
                                        swipe,
                                        autoplay,
                                        prev,
                                        next,
                                        pagitation,
                                        responsive = false;

                                if (carousel.hasClass('no-swipe')) {
                                    swipe = false;
                                } else {
                                    swipe = true;
                                }

                                if (carousel.attr('data-carousel-autoplay') == 'true') {
                                    autoplay = true;
                                } else {
                                    autoplay = false;
                                }

                                if (carousel.attr('data-carousel-nav') == 'false') {
                                    next = false;
                                    prev = false;
                                    carousel.addClass('no-nav');
                                } else {
                                    next = carousel.find('.next');
                                    prev = carousel.find('.prev');
                                    carousel.removeClass('no-nav');
                                }

                                if (carousel.attr('data-carousel-pagination') == 'true') {
                                    pagination = carousel.find('.pagination');
                                    carousel.removeClass('no-pagination');
                                } else {
                                    pagination = false;
                                    carousel.addClass('no-pagination');
                                }

                                if (carousel.attr('data-carousel-one') == 'true') {
                                    responsive = true;
                                }

                                $(this).carouFredSel({
                                    onCreate: function() {
                                        $(window).on('resize', function(event) {
                                            event.stopPropagation();
                                        });
                                    },
                                    auto: autoplay,
                                    width: '100%',
                                    infinite: false,
                                    next: next,
                                    prev: prev,
                                    pagination: pagination,
                                    responsive: responsive,
                                    swipe: {
                                        onMouse: false,
                                        onTouch: swipe
                                    },
                                    scroll: 1
                                }).parents('.carousel-box').removeClass('load');
                            });
                        }
                    }
                };
            }]);