angular.module('grsweb.common')
        .directive('mainMenu', ['$rootScope', function($rootScope) {
                return {
                    restrict: 'AE',
                    replace: true,
                    templateUrl: 'common/directive/mainmenu/menu.html',
                    link: function($scope) {
                        var $ = jQuery,
                                body = $('body'),
                                primary = '.primary';

                        $(primary).find('.parent > a .open-sub, .megamenu .title .open-sub').remove();

                        if ((body.width() + scrollWidth) <= 979) {
                            $(primary).find('.parent > a, .megamenu .title').append('<span class="open-sub"><span></span><span></span></span>');
                        } else {
                            $(primary).find('ul').removeAttr('style').find('li').removeClass('active');
                        }

                        $(primary).find('.open-sub').click(function(event) {
                            event.preventDefault();

                            var item = $(this).closest('li, .box');

                            if ($(item).hasClass('active')) {
                                $(item).children().last().slideUp(600);
                                $(item).removeClass('active');
                            } else {
                                var li = $(this).closest('li, .box').parent('ul, .sub-list').children('li, .box');

                                if ($(li).is('.active')) {
                                    $(li).removeClass('active').children('ul').slideUp(600);
                                }

                                $(item).children().last().slideDown(600);
                                $(item).addClass('active');
                            }
                        });

                        $(primary).find('.parent > a').click(function(event) {
                            if (((body.width() + scrollWidth) > 979) && (navigator.userAgent.match(/iPad|iPhone|Android/i))) {
                                var $this = $(this);

                                if ($this.parent().hasClass('open')) {
                                    $this.parent().removeClass('open')
                                } else {
                                    event.preventDefault();

                                    $this.parent().addClass('open')
                                }
                            }
                        });
                        body.on('click', function(event) {
                            if (!$(event.target).is(primary + ' *')) {
                                if ($(primary + ' .collapse').hasClass('in')) {
                                    $(primary + ' .navbar-toggle').addClass('collapsed');
                                    $(primary + ' .navbar-collapse').collapse('hide');
                                }
                            }
                        });
                        $("body").on("click", function(event) {
                            var target = $(event.target);
                            if (!$('.nav-collapse').hasClass('visible') && !target.is('a.btn-default') && !target.is('a.dropdown-toggle')) {
                                $scope.navVisibility = false;
                            }
                            $scope.$apply();
                        });
                        var topMenu = $('.top-navbar').find('.collapse');

                        if ((body.width() + scrollWidth) < 768) {
                            topMenu.css('width', body.width());
                        } else {
                            topMenu.css('width', 'auto');
                        }
                    }
                };
            }]);

        