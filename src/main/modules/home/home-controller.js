angular.module('grsweb.modules.home', [])
        .controller('homeCtrl', function($scope) {
            $scope.$parent.seo = {
                pageTitle: 'GRS Home',
                pageDescripton: 'Welcome - GRS Sales Corporation'
            };
        });