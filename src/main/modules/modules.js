angular.module('grsweb.modules', [
    'grsweb.modules.aboutus',
    'grsweb.modules.brands',
    'grsweb.modules.careers',
    'grsweb.modules.contactus',
    'grsweb.modules.home',
    'grsweb.modules.showroom',
    'grsweb.modules.history',
    'grsweb.modules.products'
]);