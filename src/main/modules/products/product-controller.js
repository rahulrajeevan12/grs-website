angular.module('grsweb.modules.products', [])
        .controller('productDetailCtrl', ['$scope', '$routeParams', 'Product',
            function($scope, $routeParams, Products) {
                $scope.product = Products.get({productId: $routeParams.productId}, function(product) {
                    $scope.mainImageUrl = product.images[0];
                });

                $scope.setImage = function(imageUrl) {
                    $scope.mainImageUrl = imageUrl;
                };
            }])

        .controller('productListCtrl', ['$scope', 'Product',
            function($scope, Product) {
                $scope.products = Product.query();
                $scope.orderProp = 'age';
                $scope.$parent.seo = {
                    pageTitle: 'Product Details',
                    pageDescripton: 'About GRS Sales Corpotation'
                };
            }]);